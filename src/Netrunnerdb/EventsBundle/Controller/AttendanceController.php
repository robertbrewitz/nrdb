<?php
namespace Netrunnerdb\EventsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Netrunnerdb\EventsBundle\Entity\Attendance;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Netrunnerdb\EventsBundle\Entity\Event;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AttendanceController extends Controller
{
    public function registerAction($event_id, Request $request)
    {
    	$user = $this->getUser();

    	$em = $this->getDoctrine()->getManager();
    	/* @var $event Event */
    	$event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($event_id);
    	
    	if(!$event) {
    		throw BadRequestHttpException("Unable to find the event.");
    	}
    	
    	$attendance = $em->getRepository('NetrunnerdbEventsBundle:Attendance')->findOneBy(array('event' => $event, 'user' => $user));
    	
    	if(!$attendance) {
    		$attendance = new Attendance();
    		$attendance->setUser($user);
    		$attendance->setEvent($event);
    		$attendance->setTs(new \DateTime());
    		$attendance->setPlayername($user->getUsername());
    		$attendance->setIsBye(FALSE);
    		$attendance->setIsDisqualified(FALSE);
    		$attendance->setIsForfeit(FALSE);
    		$attendance->setIsValidated(FALSE);
    		
    		$em->persist($attendance);
    		
    		$event->setNbplayers(count($event->getAttendances()) + 1);
    		$em->flush();
    	}
    	
    	return $this->redirect($this->generateUrl('event_view', array('_locale' => $this->getRequest()->getLocale(), 'event_id' => $event_id)));
    }
    
    public function cancelAction($event_id, Request $request)
    {
    	$user = $this->getUser();
    
    	$em = $this->getDoctrine()->getManager();
    	/* @var $event Event */
    	$event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($event_id);
    	 
    	if(!$event) {
    		throw BadRequestHttpException("Unable to find the event.");
    	}
    	 
    	$attendance = $em->getRepository('NetrunnerdbEventsBundle:Attendance')->findOneBy(array('event' => $event, 'user' => $user));
    	 
    	if($attendance) {
    		$em->remove($attendance);
    		$event->setNbplayers(count($event->getAttendances()) - 1);
    		$em->flush();
    	}
    	 
    	return $this->redirect($this->generateUrl('event_view', array('_locale' => $this->getRequest()->getLocale(), 'event_id' => $event_id)));
    }
    
    public function validateAction($attendance_id, Request $request)
    {
    
    }
    
    public function rejectAction($attendance_id, Request $request)
    {
    
    }
    
    public function addPlayerAction($event_id, Request $request)
    {
        $user = $this->getUser();
        
        $em = $this->getDoctrine()->getManager();
        
        /* @var $event Event */
        $event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($event_id);
        
        if(!$event) {
            throw BadRequestHttpException("Unable to find the event.");
        }
        if($event->getUser()->getId() !== $user->getId()) {
            throw UnauthorizedHttpException("You cannot manage this event.");
        }
        
        $attendance = new Attendance();
        $attendance->setPlayername($request->request->get('playername'));
        $attendance->setIsBye((boolean) $request->request->get('is_bye'));
        $attendance->setIsDisqualified((boolean) $request->request->get('is_disqualified'));
        $attendance->setIsForfeit((boolean) $request->request->get('is_forfeit'));
        $attendance->setIsValidated(TRUE);
        $attendance->setPrestige(intval($request->request->get('prestige')));
        $attendance->setRank(intval($request->request->get('rank')));
        $attendance->setSos(intval($request->request->get('sos')));
        $attendance->setCorpIdentity($em->getRepository('NetrunnerdbCardsBundle:Card')->find(intval($request->request->get('corp_identity_id'))));
        $attendance->setRunnerIdentity($em->getRepository('NetrunnerdbCardsBundle:Card')->find(intval($request->request->get('runner_identity_id'))));
        $attendance->setEvent($event);
        $attendance->setTs(new \DateTime);
        $em->persist($attendance);
        
        $event->setNbplayers(count($event->getAttendances()) + 1);
        
        $em->flush();
        $em->refresh($event);
        
        $type_identity = $em->getRepository('NetrunnerdbCardsBundle:Type')->findBy(array('name' => "Identity"));
        $side_corp = $em->getRepository('NetrunnerdbCardsBundle:Side')->findOneBy(array('name' => "Corp"));
        $side_runner = $em->getRepository('NetrunnerdbCardsBundle:Side')->findOneBy(array('name' => "Runner"));
        $corps = $em->getRepository('NetrunnerdbCardsBundle:Card')->findBy(array('type' => $type_identity, 'side' => $side_corp), array('code' => 'ASC'));
        $runners = $em->getRepository('NetrunnerdbCardsBundle:Card')->findBy(array('type' => $type_identity, 'side' => $side_runner), array('code' => 'ASC'));
        
        return $this->render('NetrunnerdbEventsBundle:Event:event_view_past.html.twig', array(
            'event' => $event,
            'corps' => $corps,
            'runners' => $runners
        ));
    }
}